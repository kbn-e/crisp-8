# Please use bmake
.MAIN: build

CC ?= cc	# should allow us to have a little flexibility 
CFLAGS += -I/usr/local/include -I/usr/include -fno-common -Iinclude
LDFLAGS = -L/usr/local/lib -lxcb -Xlinker --allow-multiple-definition

.if make (build)
CFLAGS += -Oz 
LDFLAGS += -s 
.elif make (debug)
CFLAGS += -g -v -ftime-report
.endif

SRCS = src/main.c src/cpu.c src/storge.c src/gui.c
OBJ = src/main.o src/cpu.o src/storge.o src/gui.o

depend:
	mkdep ${CFLAGS} ${SRCS}

build: ${OBJ}
	${CC} ${OBJ} ${CFLAGS} ${LDFLAGS} -o crispy

dbg: ${OBJ}
	${CC} ${OBJ} ${CFLAGS} ${LDFLAGS} -o ${.TARGET}

clean:
	rm -f ${OBJ} crispy dbg 
