/*
Table of sprits
**** 	11110000 0xF0	  *	00100000 0x20 	****	11110000 0xF0
*  *	10010000 0x90	 **	01100000 0x60      *	00010000 0x10
*  *	10010000 0x90	  *	00100000 0x20 	****	11110000 0xF0
*  *	10010000 0x90	  *	00100000 0x20 	*	10000000 0x80
****	11110000 0xF0	 ***	01110000 0x70 	****	11110000 0xF0

****	11110000 0xF0	*  *	10010000 0x90	****	11110000 0xF0
   *	00010000 0x10	*  *	10010000 0x90	*	10000000 0x80
****	11110000 0xF0	****	11110000 0xF0	****	11110000 0xF0
   *	00010000 0x10      *	00010000 0x10	   *	00010000 0x10
****	11110000 0xF0      *	00010000 0x10	****	11110000 0xF0
                                        
****	11110000 0xF0   ****	11110000 0xF0	****	11110000 0xF0
*	10000000 0x80      *	00010000 0x10	*  *	10010000 0x90
****	11110000 0xF0     *	00100000 0x20	****	11110000 0xF0
*  *	10010000 0x90    *	01000000 0x40	*  *	10010000 0x90
****	11110000 0xF0    *	01000000 0x40	****	11110000 0xF0

****	11110000 0xF0 	****	11110000 0xF0	*** 	11100000 0xE0
*  *	10010000 0x90 	*  *	10010000 0x90	*  *	10010000 0x90
****	11110000 0xF0	****	11110000 0xF0	****	11110000 0xF0
   *	00010000 0x10	*  *	10010000 0x90	*  *	10010000 0x90
****	11110000 0xF0 	*  *	10010000 0x90	***	11100000 0xE0

****	11110000 0xF0	*** 	11100000 0xE0	****	11110000 0xF0
*	10000000 0x80   *  *	10010000 0x90	*	10000000 0x80
*	10000000 0x80	*  *	10010000 0x90	****	11110000 0xF0
*	10000000 0x80	*  *	10010000 0x90	*	10000000 0x80
****	11110000 0xF0	*** 	11100000 0xE0	****	11110000 0xF0

****	11110000 0xF0
*	10000000 0x80
****	11110000 0xF0
*	10000000 0x80
*	10000000 0x80

*/

#ifndef GUI_H
#define GUI_H

#include<stdint.h>

typedef struct buff{ 
 uint8_t x[0xF];
} buffer_t;

#ifdef _TUI

#include<curses.h>

#else
#ifdef __unix

#include<xcb/xcb.h>

extern xcb_connection_t * connection;
extern xcb_setup_t *setup;
extern xcb_screen_t *screen;
extern xcb_drawable_t window;
extern xcb_gcontext_t foreground;

#define conec() xcb_connection_t * connection = xcb_connect(NULL, NULL); \
const xcb_setup_t *setup = xcb_get_setup(connection); \
xcb_screen_t *screen = (xcb_setup_roots_iterator(setup)).data;

#elif _Win32
#endif
#endif // _TUI

void draw(uint8_t* vx,uint8_t* vy,uint8_t n);

#endif
