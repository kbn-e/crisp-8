#ifndef _STORGE_H_
#define _STORGE_H_ 

#define RAM_END 4096

#include<stdio.h>
#include<stdint.h>

uint8_t RAM[RAM_END];

extern uint16_t PC;

uint16_t IR; 
uint8_t delytmr; 
uint8_t sndtmr;
uint8_t V[0x10];

struct stack{
 uint16_t arry[16];
 uint16_t count;
// static_assert(StPtr > &arry[15],"stack pointer exceding bounds");
}; 

#define STACK_TOP stacki.arry
extern uint8_t StPtr; 
/*
 flag will be used to determine if a variable is currently being used.
 this will be set & unset by the push & pop respectively.
 and since we only need one stack we'll be statically defining it 
*/

void push(uint16_t); 
uint16_t pop();

#endif
