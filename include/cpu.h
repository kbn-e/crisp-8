#ifndef _CPU_H
#define _CPU_H

#include<gui.h>
#include<stdint.h>

/*
00E0 clr
00EE ret 
1nnn jmp nnn
2nnn call sub
3nnn if vx != NN then
4nnn if vx == NN then
5nnn if vx != vy then 
*/

typedef struct disig {
	uint8_t flag;
	buffer_t cell;
} signal_t;
signal_t doin(uint16_t);
// uint8_t rand();

#endif
