[![CircleCI](https://dl.circleci.com/status-badge/img/circleci/HaDkSvaTZVk5kQxYw2LyqZ/FN8TD67Z6rmrEt7Hb2Ugy8/tree/main.svg?style=svg)](https://dl.circleci.com/status-badge/redirect/circleci/HaDkSvaTZVk5kQxYw2LyqZ/FN8TD67Z6rmrEt7Hb2Ugy8/tree/main)

## Crisp-8
Yet another Chip-8 implimentation this time in C.
This implimentation isn't too rigid in following the original specifications.

### Building
The make used here is FreeBSD's make though it should work with bmake.
Currently doesn't build or run on non-unix platforms; not that I have
created a limitation that functionally prevents it from being run or built.

#### Release
`make depend`
`make`

#### Debug
`make depend`
`make dbg`

## LICENCE
Copyright 2022, Upendra Kamath
Released under [BSD 2 Clause licence](LICENCE).
